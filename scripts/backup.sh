#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: backup.sh
# Author: Philippe Heyvaert
# Date Created: 10-10-2024 CEST
# Date Modified: 02-11-2024 CEST
# Dependencies: tar
# Usage: Backup files.
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

# Load settings from external file.
CONFIG_FILE="/home/philippe/backup.conf"

if [ -f "$CONFIG_FILE" ]; then
    # echo "Configuration file found!"
    source "$CONFIG_FILE"
else
    echo "There is no configuration file!"
    exit 1
fi

# Script variables.
BACKUP_DATE=$(date +%d%m%Y%I%M%S)
BACKUP_FILENAME="backup_$BACKUP_DATE.tar.gz"
LOG_FILE="$BACKUP_DST/backup_$BACKUP_DATE.log"

# Create the directory.
mkdir -p "$BACKUP_DST/backup_$BACKUP_DATE"

# Start logging the backup
exec >> >(tee -a "$LOG_FILE") 2>&1
tar -czf "$BACKUP_DST/backup_$BACKUP_DATE/$BACKUP_FILENAME" -C "$BACKUP_SRC" .

# Verify if the backup-file was succesful.
if [ $? -eq 0 ]; then
    echo "Backup succesful: $BACKUP_FILENAME"
else
    echo "Backup failed!"
    exit 1
fi