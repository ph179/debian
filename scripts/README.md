# My bash scripts

1. [Simple Script](#simple-script)
1. [Backup Script](#backup-script)
1. [While loop](#example-of-a-while-loop)

---
## Simple script

Hello, World!

```bash
#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: hello.sh
# Author: Philippe Heyvaert
# Date Created: 28-08-2024 CEST
# Date Modified: 28-08-2024 CEST
# Dependencies: None
# Usage: Bash Scripting Fun
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

string="Hello, World!"
echo "$string"
```

![](img/make_script_executable.mp4)

## Backup script

1. backup.sh

I'm trying to build a script to handle my backups. I want to use an 
external file for my configuration settings. So step 1 will be checking 
if that file exists.

My bash code so far:

```bash
#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: backup.sh
# Author: Philippe Heyvaert
# Date Created: 10-10-2024 CEST
# Date Modified:
# Dependencies: None
# Usage: Backup files.
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

# Load settings from external file.
CONFIG_FILE="/home/philippe/backup.conf"

if [ -f "$CONFIG_FILE" ]; then
    source "$CONFIG_FILE"
else
    echo "There is no configuration file!"
    exit 1
fi
```

![](img/backup.mp4)

2. backup.conf

Now since we don't have an external configuration file yet, let us make one. 
Remember to store it in your home folder or change the folder location in the 
script (CONFIG_FILE="...").

![](img/vim_usage.mp4)

3. backup.conf

Now when I was rewatching the movieclip on my Gitlab I've noticed my folder for 
the backups destination was wrong. So let's correct it quickly using vim.

![](img/vim_usage_0.mp4)

4. backup.sh

The configuration file is copied to the correct location on my computer. Let's 
modify or script with an ```echo-statement``` and find out if the scripts can 
retrieve my ```backup.conf file```. That ```echo-statement``` will be commented 
out if it works.

Modified code:

```bash
#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: backup.sh
# Author: Philippe Heyvaert
# Date Created: 10-10-2024 CEST
# Date Modified:  11-10-2024 CEST
# Dependencies: None
# Usage: Backup files.
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

# Load settings from external file.
CONFIG_FILE="/home/philippe/backup.conf"

if [ -f "$CONFIG_FILE" ]; then
    echo "Configuration file found!"
    source "$CONFIG_FILE"
else
    echo "There is no configuration file!"
    exit 1
fi
```

![](img/configuration_detected.mp4)

5. Archiving the backup

We want to archive the baclup. I'm going to use the CLI command ```tar``` for this 
in combination with the program ```gzip```. I'll test it first in the terminal on 
the ```hello.sh``` script.

The command should look like this:

```bash
tar -flags "output-file" "input-file"
```

So I'll be using ```hello.sh``` as file to test it.
My ```flags``` will be czf. You can find out about the tar command in the man pages i the terminal 
or look at those online at this [link](https://man7.org/linux/man-pages/man1/tar.1.html).

The command to access man pages in the terminal for ```tar```:

```bash
man tar
```

![tar man page](img/tar_man_page.png)

I'll be using a seperate test-folder for my movies.

The command in the terminal should look like this:

```bash
tar -czf "hello.tar.gz" "hello.sh"
```

There are 2 movie clips below. The 1st one is how to create an archive file in the termainal. 
The 2nd one is how to extract an archive in the terminal. I've deleted the ```hello.sh``` 
script before extracting the archive... of course.

The command for extracting should look like this:

```bash
tar -xzf "hello.tar.gz" "hello.sh"
```

![](img/create_archive.mp4)

![](img/extracting_archive.mp4)

6. Adding extra variables.

Now for good measure, I also want to log my backups. So I'll need some variables since I want 
to use a name for my files containing date and time. That way I can see what file is the oldest 
and what file is the latest.

This date and time name-giving is also going to be being used to check if the backup was a success 
or not in later code. Just thinking a little bit ahead now. The video shows the date-time output 
that will be used in the name-giving in the terminal. The format will be daymonthyearhourminutesseconds. 
All nice squeezed together.

Modified code:

```bash
#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: backup.sh
# Author: Philippe Heyvaert
# Date Created: 10-10-2024 CEST
# Date Modified: 15-10-2024 CEST
# Dependencies: tar
# Usage: Backup files.
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

# Load settings from external file.
CONFIG_FILE="/home/philippe/backup.conf"

if [ -f "$CONFIG_FILE" ]; then
    # echo "Configuration file found!"
    source "$CONFIG_FILE"
else
    echo "There is no configuration file!"
    exit 1
fi

# Script variables
BACKUP_DATE=$(date +%d%m%Y%I%M%S)
BACKUP_FILENAME="backup_$BACKUP_DATE.tar.gz"
LOG_FILE="$BACKUP_DST/backup_$BACKUP_DATE.log"

# Create the directory
mkdir -p "$BACKUP_DST/backup_$BACKUP_DATE"
```

![](img/date_time.mp4)

7. We've added some bash srcipting code to create our backup file. Let's try it, video below code.
I also changed my ```backup.conf``` file, so I'll be testing on my test folder first.

Modified code:

```bash
#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: backup.sh
# Author: Philippe Heyvaert
# Date Created: 10-10-2024 CEST
# Date Modified: 18-10-2024 CEST
# Dependencies: tar
# Usage: Backup files.
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

# Load settings from external file.
CONFIG_FILE="/home/philippe/backup.conf"

if [ -f "$CONFIG_FILE" ]; then
    # echo "Configuration file found!"
    source "$CONFIG_FILE"
else
    echo "There is no configuration file!"
    exit 1
fi

# Script variables
BACKUP_DATE=$(date +%d%m%Y%I%M%S)
BACKUP_FILENAME="backup_$BACKUP_DATE.tar.gz"
LOG_FILE="$BACKUP_DST/backup_$BACKUP_DATE.log"

# Create the directory
mkdir -p "$BACKUP_DST/backup_$BACKUP_DATE"

# Start logging the backup
exec >> >(tee -a "$LOG_FILE") 2>&1
tar -czf "$BACKUP_DST/backup_$BACKUP_DATE/$BACKUP_FILENAME" -C "$BACKUP_SRC"
```

![](img/backup_0.mp4)

So... something went wrong. Let's see.

- Has the backup folder been created? Yes it has.

![Backup Folder](img/folder.png)

- Has the backup folder with date and time been created? Yes it has.

![Backup Folder 0](img/folder_0.png)

- Is there a backup file in this folder? No there isn't.

![Backup Folder 1](img/folder_1.png)

- What does the log file say?

![Log file](img/backup_log.png)

- So I figured out the problem. I'm not specifying any output files to archive in the ```tar``` 
archive. I want to add all files in the directory to my ```tar``` archive. So to do is in bash 
is use the ```.``` in your code, use a space though. Let's give it a try now.

- I've tried it and it worked. So now I've added a extra file in my test folder to try and make 
a video for this script so far. As always, you can find the video below my bash code.

Modified code:

```bash
#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: backup.sh
# Author: Philippe Heyvaert
# Date Created: 10-10-2024 CEST
# Date Modified: 22-10-2024 CEST
# Dependencies: tar
# Usage: Backup files.
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

# Load settings from external file.
CONFIG_FILE="/home/philippe/backup.conf"

if [ -f "$CONFIG_FILE" ]; then
    # echo "Configuration file found!"
    source "$CONFIG_FILE"
else
    echo "There is no configuration file!"
    exit 1
fi

# Script variables
BACKUP_DATE=$(date +%d%m%Y%I%M%S)
BACKUP_FILENAME="backup_$BACKUP_DATE.tar.gz"
LOG_FILE="$BACKUP_DST/backup_$BACKUP_DATE.log"

# Create the directory
mkdir -p "$BACKUP_DST/backup_$BACKUP_DATE"

# Start logging the backup
exec >> >(tee -a "$LOG_FILE") 2>&1
tar -czf "$BACKUP_DST/backup_$BACKUP_DATE/$BACKUP_FILENAME" -C "$BACKUP_SRC" .
```

![](img/backup_1.mp4)

8. Now let's verify if the backup was created. We'll echo a message indicating the 
script result.

![](img/backup_2.mp4)

## Example of a while-loop

Here is an example of a while-loop using bash scripting. Below is the code 
is a video of the terminal output of this script.

Code:

```bash
#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: while-example.sh
# Author: Philippe Heyvaert
# Date Created: 28-08-2024 CEST
# Date Modified: 28-08-2024 CEST
# Dependencies: None
# Usage: Bash Scripting Fun
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

x=1

# -le --> Less or Equal
while [ $x -le 5 ]
do
    echo "This is the number $x."
    x=$(( $x + 1 ))
done
```

![](img/while_loop.mp4)