#!/usr/bin/env bash
#----------------------------------------------------
#    ___           __   ____        _      __
#   / _ )___  ___ / /  / __/_______(_)__  / /_
#  / _  / _ `(_-</ _ \_\ \/ __/ __/ / _ \/ __/
# /____/\_,_/___/_//_/___/\__/_/ /_/ .__/\__/
#                                 /_/
#----------------------------------------------------
# Filename: hello.sh
# Author: Philippe Heyvaert
# Date Created: 28-08-2024 CEST
# Date Modified: 28-08-2024 CEST
# Dependencies: None
# Usage: Bash Scripting Fun
# License: GNU/GPLv3 (or later)
#          https://www.gnu.org/licenses/gpl-3.0.html
#----------------------------------------------------

string="Hello, World!"
echo "$string"
