# Welcome to my Gitlab page

Here are my sections so far:

---
### Debian

![/img/debian_logo.png](https://gitlab.com/ph179/debian/-/tree/main?ref_type=heads)

Everything related to my GNU/Linux Debian setup.

1. [My Desktop](#my-desktop)
2. [My Terminal](#my-terminal)
3. [My Internet Browser](#my-internet-browser)
4. [My File Manager](#my-file-manager)
5. [My Vim Setup](#my-vim-setup)

Terminal Magic:

1. [Git](#basic-git-usage)
2. [Video Conversion](#video-conversion)
3. [Fancy ASCII](#fancy-ascii-stuff-with-figlet)
4. [Cow Fun](#cow-fun)
5. [Christmas](#christmas-in-my-gnulinux-desktop)
6. [Updating my Debian system](#updating-my-debian-system)
7. [Cmatrix](#cmatrix)
8. [Timeshift](#timeshift)

Gaming:

1. [0AD (Zero A.D.)](#0AD)
2. [Risk Global Domination on Steam](#Risk)

---
### [Python](https://gitlab.com/ph179/python/-/tree/main?ref_type=heads)

All my Python files and projects I'm working on or finished.

---
### [Wallpapers](https://gitlab.com/ph179/wallpapers/-/tree/main?ref_type=heads)

My large wallpaper collection, feel free to browse my collection and download what you like.

---
# My system: Debian 12 (Bookworm)

## GNU/Linux systems

I've tried several... but Debian is my favorite by far. It is stable and reliable. Any issues with older packages can be resolved by using flatpacks (like most Linux users I'm not a fan of snaps).

---
## My desktop

I've configured my XFCE desktop with 4 workspaces and some widgets I like.

![XFCE Desktop](img/desktop.png)

## My terminal

I use the standard XFCE terminal, and I made it look like this:

![XFCE Terminal](img/xfce_terminal.png)

## My Internet Browser

I use Mozilla Firefox, I've added several Add-ons. My theme is 'Dracula'.

![Firefox Browser](img/internet_browser.png)

## My File Manager

I use the standard XFCE file manager Thunar. It works fine for me and here is my tweaked version:

![Thunar](img/thunar.png)

## My Vim Setup

I've been tweaking Vim for many years now, stealing from other users and creating my own setup. This is how it looks like:

![Vim](img/vim_config.png)

### Terminal Magic

The terminal is a powerful tool in GNU/Linux... if you know how to use it.

Some examples:

#### Basic git usage

![](img/git_usage.mp4)

#### Video Conversion

![](img/terminal_video_conversion.mp4)

#### Fancy [ASCII](http://www.asciitabel.be/) stuff with figlet

![](img/figlet.mp4)

#### Cow fun

![](img/cow_fun.mp4)

#### Christmas in my GNU/Linux desktop

![](img/christmas.mp4)

#### Updating my Debian system

![](img/debian_update.mp4)

#### Cmatrix

![](img/cmatrix.mp4)

#### Timeshift

Terminal view:

![](img/timeshift_cli.mp4)

GUI view:

![](img/timeshift_gui.mp4)

### Gaming

0AD (Zero A.D.):

![](img/0ad.mp4)

Risk Global Domination on Steam:

![](img/risk.mp4)

![](/img/risk_0.mp4)